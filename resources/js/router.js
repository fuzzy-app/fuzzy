import VueRouter from 'vue-router';
import Homepage from "./views/Homepage";
import Vue from 'vue';

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'homepage',
            component: Homepage
        },

    ],
});

export default router;
