import ApiService from "./api.service";
import {TokenService} from "./storage.service";

class AuthenticationError extends Error {
    constructor(errorCode, message) {
        super(message);
        this.name = this.constructor.name;
        this.message = message;
        this.errorCode = errorCode;
    }
}

const UserService = {
    login: async function(email, password) {
        const requestData = {
            method: 'post',
            url: "/api/login",
            data: {
                email: email,
                password: password
            },
        };

        try {
            const response = await ApiService.customRequest(requestData);

            TokenService.saveToken(response.data.token);
            TokenService.saveRefreshToken(response.data.token);
            ApiService.setHeader();

            ApiService.mount401Interceptor();

            return response.data;
        } catch (error) {
            throw new AuthenticationError(error.response.data.status, error.response.data.errors)
        }
    },

    /**
     * Register the user and store the access token to TokenService.
     *
     * @returns access_token
     * @throws AuthenticationError
     **/
    register: async function(name, email, password) {
        const requestData = {
            method: 'post',
            url: "/api/register",
            data: {
                name: name,
                email: email,
                password: password
            }
        };

        try {
            const response = await ApiService.customRequest(requestData);

            TokenService.saveToken(response.data.token);
            TokenService.saveRefreshToken(response.data.token);
            ApiService.setHeader();

            ApiService.mount401Interceptor();

            return response.data;
        } catch (error) {
            throw new AuthenticationError(error.response.status, error.response.data.errors);
        }
    },

    /**
     * Logout the current user by removing the token from storage.
     *
     * Will also remove `Authorization Bearer <token>` header from future requests.
     **/
    logout() {
        // Remove the token and remove Authorization header from Api Service as well
        TokenService.removeToken();
        TokenService.removeRefreshToken();
        ApiService.removeHeader();
        // NOTE: Again, we'll cover the 401 Interceptor a bit later.
        ApiService.unmount401Interceptor()
    },

    /**
     * Get logged in User
     **/
    getUser: async function() {
        const requestData = {
            method: 'get',
            url: "/api/user/",
        };
        try {
            const response = await ApiService.customRequest(requestData);

            return response.data.data;
        } catch (error) {
            throw new AuthenticationError(error.response.data.status, error.response.data.errors)
        }

    },
};

export default UserService

export { UserService, AuthenticationError }
