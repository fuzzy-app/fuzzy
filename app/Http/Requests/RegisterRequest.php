<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'email' => 'required|string|unique:users',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ];
    }

    /**
     * Get custom messages for validator errors
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name' => 'Email param is required',
            'email' => 'Email param is required',
            'password' => 'Password param is required',
            'remember_me' => 'Remember Me param is required'
        ];
    }
}
